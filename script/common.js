function fnReady() {
    fnReadyKeyback();
    fnReadyOpenWin();
    fnReadyHeader();
    fnReadyFooter();
};

function getHeaderHeight() {
    var header = $api.byId('header');
    $api.fixIos7Bar(header);
    headerHeight = $api.offset(header).h;
    return headerHeight;
}
function getFooterHeight() {
    var footer = $api.byId('footer');
    // $api.fixIos7Bar(footer);
    footerHeight = $api.offset(footer).h;
    return footerHeight;
}
function select1(id) {
  var selects1 = document.getElementById(id);
   var indexs = selects1.selectedIndex;
   let values1 = selects1.options[indexs].value;
   return values1;
}
function select2(id) {
  var selects = document.getElementById(id);
   var indexs = selects.selectedIndex;
   let values1 = selects.options[indexs].text;
   return values1;
}

function openActDetail(id) {
  api.openWin({
      name: 'backHeader',
      url: './backHeader.html',
      slidBackEnabled: false,
      bounces: false,
      pageParam: {
        id: id
      }
  });
  // api.closeWin();
}

function fnReadyKeyback() {
    var keybacks = $api.domAll('.event-back');
    for (var i = 0; i < keybacks.length; i++) {
        $api.attr(keybacks[i], 'tapmode', 'highlight');
        keybacks[i].onclick = function() {
            api.closeWin();
        };
    }

    api.parseTapmode();
};


function fnReadyOpenWin() {
    var buttons = $api.domAll('.open-win');
    for (var i = 0; i < buttons.length; i++) {
        $api.attr(buttons[i], 'tapmode', 'highlight');
        buttons[i].onclick = function() {
            var target = $api.closest(event.target, '.open-win');
            var winName = $api.attr(target, 'win'),
                isNeedLogin = $api.attr(target, 'register_mobile'),
                param = $api.attr(target, 'param');

            if (isNeedLogin && !$api.getStorage('loginInfo')) {
                winName = 'register_mobile';
            }

            if (param) {
                param = JSON.parse(param);
            }

            api.openWin({
                name: winName,
                url: './' + winName + '.html',
                pageParam: param
            });
        };
    }
    api.parseTapmode();
};

var header, headerHeight = 0;

function fnReadyHeader() {
  // console.log('3333');
    header = $api.byId('header');
    if (header) {
        $api.fixIos7Bar(header);
        headerHeight = $api.offset(header).h;
    }
};

function fnReadyFooter() {
  // console.log('3333');
    footer = $api.byId('footer');
    if (footer) {
        $api.fixIos7Bar(footer);
        footerHeight = $api.offset(footer).h;
    }
};
var changeFrameInRoot = function(index){
    api.execScript({
        name: 'root',
        script: 'changeIndexFrame('+index+')'
    });
};
var openCommon = function(name,title){
    api.execScript({
        name: 'root',
        script: 'indexOpenCommon("'+name+'","'+title+'")'
    });
}
function fnReadyFrame() {
    var frameName = api.winName;
    api.openFrame({
        name: frameName,
        url: './' + frameName + '.html',
        bounces: true,
        rect: {
            x: 0,
            y: headerHeight,
            w: 'auto',
            h: 'auto'
        },
        vScrollBarEnabled: false
    });
};

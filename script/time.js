function getNowFormatDate() {
       var date = new Date();
       var seperator1 = "-";
       var year = date.getFullYear();
       var month = date.getMonth() + 1;
       var strDate = date.getDate();
       if (month >= 1 && month <= 9) {
           month = "0" + month;
       }
       if (strDate >= 0 && strDate <= 9) {
           strDate = "0" + strDate;
       }
       var currentdate = year + seperator1 + month + seperator1 + strDate;
       return currentdate;
   }

   function getDate(ret) {
          var seperator1 = "-";
          if (ret.month >= 1 && ret.month <= 9) {
              ret.month = "0" + ret.month;
          }
          if (ret.day >= 0 && ret.day <= 9) {
              ret.day = "0" + ret.day;
          }
          var currentdate = ret.year + seperator1 + ret.month + seperator1 + ret.day;
          return currentdate;
      }

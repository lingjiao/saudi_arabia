(function($) {
        $.init();
        // var result = $('#result')[0];
        var fromTime = document.getElementById('fromTime')
        var toTime = document.getElementById('toTime')
          fromTime.addEventListener('tap', function() {
            var optionsJson = this.getAttribute('data-options') || '{}';
            var options = JSON.parse(optionsJson);
            var id = this.getAttribute('id');
            var picker = new $.DtPicker(options);
            picker.show(function(rs) {
              fromTime.value = rs + ':00';
              picker.dispose();
            });
        });
        toTime.addEventListener('tap', function() {
          var optionsJson = this.getAttribute('data-options') || '{}';
          var options = JSON.parse(optionsJson);
          var id = this.getAttribute('id');
          var picker = new $.DtPicker(options);
          picker.show(function(rs) {
            toTime.value = rs + ':00';
            picker.dispose();
          });
      });
      })(mui);

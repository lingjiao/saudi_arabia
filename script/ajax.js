//  获取用户信息
function getUser(id, interText) {
    api.ajax({
      url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/sysUserLogin/app/findData.action',
      method: 'get',
      // cache: true,
      data: {
        values: {
          userId: id
        }
      }
    }, function(ret, err) {
        if (ret) {
          profile.innerHTML = interText(ret.data);
          // $api.setStorage('userData', ret.data);
        } else {
          // api.alert({ msg: JSON.stringify(err.msg) });
          alert('Server exception');
        }
    });
}
// 用户退出
function Logout() {
  api.ajax({
    url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/sysUserLogin/app/logout.action',
    method: 'post',
    data: {}
}, function(ret, err) {
    if (ret.data === true) {
      $api.rmStorage('query');
      // console.log('1')
      // api.openWin({
      //     name: 'index',
      //     url: '../index.html',
      //     bounces: false,
      //     slidBackEnabled: false
      // });
      api.openFrame({
          name: 'index',
          url: '../index.html',
          rect: {
              x: 0,
              y: 0,
              w: 'auto',
              h: 'auto'
          }
      });
      api.closeFrame();
      // api.closeWin();
      // console.log(JSON.stringify(ret));
      // profile.innerHTML = interText(ret.data);
        // api.alert({ msg: JSON.stringify(ret) });
    } else {
        // api.alert({ msg: JSON.stringify(err.msg) });
        alert('Server exception');
    }
  })
}
//  垃圾桶在线情况
function getGarbage(interText) {
  api.ajax({
    url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/GarbageBin/app/findData.action',
    method: 'get',
    data: {}
    }, function(ret, err) {
    if (ret) {
      muiContent.innerHTML = interText(ret.data);
        // api.alert({ msg: JSON.stringify(ret) });
    } else {
      // console.log(JSON.stringify(err))
      alert('Server exception');
        // api.alert({ msg: JSON.stringify(err.msg) });
    }
  });
}
//  车辆在线情况
function getVehicle(interText) {
  api.ajax({
    url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/Vehiclemonitor/app/findData.action',
    method: 'get',
    data: {}
  }, function(ret, err) {
      if (ret) {
        muiContent.innerHTML = interText(ret.data);
        // $api.setStorage('garbageData', ret.data);
      } else {
        // console.log(JSON.stringify(err))
        alert('Server exception');
          // api.alert({ msg: JSON.stringify(err.msg) });
      }
  });
}
//  车辆车牌号获取
function getPlateNumber(id, interText2) {
  api.ajax({
    url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/carinfo/app/findList.action',
    type: 'get',
    data: {
      userId: id
    },
    }, function(ret, err) {
      if (ret) {
        muiCommon.innerHTML = interText2(ret.data);
        // getSelect();
      } else {
        alert('Server exception');
      }
  });
}
//  获取车辆类型id
function getTypeId(interText2) {
  api.ajax({
    url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/carinfo/app/findCarTypeList.action',
    type: 'get',
    data: {
      // userId: id
    },
    }, function(ret, err) {
      if (ret) {
        muiRow2.innerHTML = interText2(ret.data);
        // getSelect();
      } else {
        alert('Server exception');
      }
  });
}
//  获取组织ID
function getId(interText) {
  api.ajax({
    url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/Organization/app/findList.action',
    type: 'get',
    data: {},
    }, function(ret, err) {
      if (ret) {
        muiContent.innerHTML = interText(ret.data);
        // getSelect();
      } else {
        alert('Server exception');
      }
  });
}
//  车辆数据
function getData (org, val2) {
  api.ajax({
    url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/Vehiclemonitor/app/findConstantInfo.action',
    type: 'get',
    data: {
      values: {
        Organization: org,  // 组织id
        PlateNumber: val2   //  车牌号
      }
    },
    }, function(ret, err) {
      if (ret) {
        initMap(ret.data);
      } else {
        alert('Server exception');

        // alert(JSON.stringify(err.msg));
      }
  });
}
  //  地图数据展示
function initMap(data) {
  let maps = $api.getStorage('maps');
  let wgs = wgs84togcj02(maps.Y, maps.X);
  let gcj = gcj02tobd09(wgs[0], wgs[1]);
  let bd0 = bd09togcj02(gcj[0], gcj[1]);
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: Number(maps.zoom),
    center: new google.maps.LatLng(bd0[0], bd0[1])
  });
  if (typeof data === undefined) return false;
  var marker, position, icons;
  for (var key in data) {
    if(data.length === 0) {
      return false;
    }
    var contentString = '<div id="content">'+
      '<div id="siteNotice">PlateNumber：'+data[key].PlateNumber +'</div>'+
      '<div id="siteNotice">Driver：'+data[key].Driver +'</div>'+
      '<div id="siteNotice">Organization：'+data[key].Organization +'</div>'+
      '<div id="siteNotice">Speed：'+data[key].Speed +'</div>'+
      '<div id="siteNotice">LastOnlineTime：'+data[key].Reporttime +'</div>'+
      '</div>';
      let wgs = wgs84togcj02(data[key].Y, data[key].X);
      let gcj = gcj02tobd09(wgs[0], wgs[1]);
      let bd0 = bd09togcj02(gcj[0], gcj[1]);
      position = new google.maps.LatLng(bd0[0], bd0[1]);
    var infowindow = new google.maps.InfoWindow({
      content: contentString,
      // maxWidth: 200,
      position: position
    });
    var marker = new google.maps.Marker({
       position:  position,
       map: map,
       icon: '../image/baibi.png'
    });

    headerClick(infowindow, marker);
  }
  function headerClick (infowindow, marker) {
    // infowindow.close(infowindow, marker);
    marker.addListener('click', function(e) {
      infowindow.open(infowindow, marker);
    });
  }
}

//  车辆列表-更多
function loadMores (mescroll, org, number, interText, page, pageSize, successCallback) {
  api.ajax({
  url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/Vehiclemonitor/app/findInfo.action',
  method: 'get',
  data:{
    values: {
      page: page,
      Organization: org,
      PlateNumber: number,
      row: pageSize
    }
  }
  },function (ret, err) {
    if(ret) {
      if (ret.data.page === ret.data.totalpage) {
        tabview.innerHTML = interText(ret.data.rows);
        mescroll.endByPage(pageSize, ret.data.totalpage);
      } else {
        mescroll.endByPage(pageSize, ret.data.totalpage);
        tabview.innerHTML += interText(ret.data.rows);
        successCallback(ret.data.rows)
      }
      // mui('#refreshContainer').pullRefresh().endPullupToRefresh((++page > ret.data.totalpage));//参数为true代表没有更多数据了。
    } else {
      mescroll.endErr();
      alert('Server exception');
    }
  // }
});
}
function loadMore2 (mescroll, org, full, clean, equiment, interText, page, pageSize, successCallback) {
  api.ajax({
    url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/SmartBin/app/findInfo.action',
    method: 'get',
    data: {
      values: {
        page: page,
        Organization: org,
        Full_degreeStatus: full,
        CleanedStatus: clean,
        EquipmentStatus: equiment,
        row: pageSize
        // total: pageIndex
      }
    }
    }, function(ret, err) {
      if (ret) {
        if (ret.data.page < ret.data.totalpage) {
          // mescroll.endByPage(pageSize, ret.data.totalpage);
          tabview.innerHTML += interText(ret.data.rows);
          successCallback(ret.data.rows);
          // tabview.innerHTML = interText(ret.data.rows);
          // mescroll.endByPage(pageSize, ret.data.totalpage);
        } else {
          mescroll.endByPage(pageSize, ret.data.totalpage);
          tabview.innerHTML += interText(ret.data.rows);
          // successCallback(ret.data.rows)
        }
      } else {
        mescroll.endErr();
        alert('Server exception');
          // api.alert({ msg: JSON.stringify(err.msg) });
      }
  });
}

function loadMore3 (mescroll, org, full, clean, equiment, interText, page, pageSize, successCallback) {
  api.ajax({
    url: 'http://223.255.10.50:8029/dsp-ShateApp-ui/GarbageBin/app/findInfo.action',
    method: 'get',
    data: {
      values: {
        page: page,
        Organization: org,
        Full_degreeStatus: full,
        CleanedStatus: clean,
        EquipmentStatus: equiment,
        row: pageSize
        // total: pageIndex
      }
    }
    }, function(ret, err) {
      if (ret) {
          if (ret.data.page === ret.data.totalpage) {
            tabview.innerHTML = interText(ret.data.rows);
            mescroll.endByPage(pageSize, ret.data.totalpage);
          // successCallback(ret.data.rows)
        } else {
          // mescroll.endByPage(pageSize, ret.data.totalpage);
          tabview.innerHTML += interText(ret.data.rows);
          successCallback(ret.data.rows)
        }
      } else {
        mescroll.endErr();
        alert('Server exception');
          // api.alert({ msg: JSON.stringify(err.msg) });
      }
  });
}

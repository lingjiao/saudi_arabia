(function($) {
        $.init();
        // var result = $('#result')[0];
        var fromTime = document.getElementById('BeginDate')
        var toTime = document.getElementById('EndDate')
          fromTime.addEventListener('tap', function() {
            var optionsJson = this.getAttribute('data-options') || '{}';
            var options = JSON.parse(optionsJson);
            var id = this.getAttribute('id');
            var picker = new $.DtPicker(options);
            picker.show(function(rs) {
              fromTime.value = rs;
              picker.dispose();
            });
        });
        toTime.addEventListener('tap', function() {
          var optionsJson = this.getAttribute('data-options') || '{}';
          var options = JSON.parse(optionsJson);
          var id = this.getAttribute('id');
          var picker = new $.DtPicker(options);
          picker.show(function(rs) {
            toTime.value = rs;
            picker.dispose();
          });
      });
      })(mui);
